package org.sda.model;

import java.util.Arrays;

public class Client {
	
	private String address;
	private Address[] addresses;
	
	public Client() {
		
	}

	public Client(String address) {
		this.address = address;
	}

	public Client(Address[] addresses) {
		this.addresses = addresses;
	}



	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}
	
	@Override
	public String toString() {
		return "Client [addresses=" + Arrays.toString(addresses) + "]";
	
	}
}