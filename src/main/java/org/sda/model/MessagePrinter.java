package org.sda.model;

public class MessagePrinter {
	private String message;
	private String newMessage;

	public MessagePrinter(String newMessage) {
		this.newMessage = newMessage;
	}

	public String getNewMessage() {
		return newMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void printMessage() {
		System.out.println(message);
		System.out.println(newMessage);
	}


}
