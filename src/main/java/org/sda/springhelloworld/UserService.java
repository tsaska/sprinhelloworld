package org.sda.springhelloworld;

import java.util.List;
import java.util.Map;

import org.sda.model.Client;
import org.sda.model.User;

public class UserService {

	private User user;
	private Client client;
	private List<Client> clients;
	private Map<User, Client> userMap;
		
	public Map<User, Client> getUserMap() {
		return userMap;
	}

	public void setUserMap(Map<User, Client> userMap) {
		this.userMap = userMap;
	}

	public UserService() {

	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public UserService(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
