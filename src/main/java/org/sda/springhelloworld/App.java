package org.sda.springhelloworld;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.sda.model.Address;
import org.sda.model.Client;
import org.sda.model.MessagePrinter;
import org.sda.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
		// exemplificare ca bean-ul este singleton, daca nu setam in beans.xml
		// scope="prototype"
		MessagePrinter messagePrinter2 = (MessagePrinter) applicationContext.getBean("messagePrinter");
		System.out.println("messagePrinter 2 value" + messagePrinter2);
		MessagePrinter messagePrinter3 = (MessagePrinter) applicationContext.getBean("messagePrinter");
		System.out.println("messagePrinter 3 value" + messagePrinter3);

		MessagePrinter messagePrinter = (MessagePrinter) applicationContext.getBean("messagePrinter");
		System.out.println(messagePrinter.getMessage());
		messagePrinter.printMessage();
		User userName = (User) applicationContext.getBean("user");
		userName.printUserName();
		System.out.println("User value with prototype scope" + userName);
		User differentUserName = (User) applicationContext.getBean("user");
		System.out.println("differentUserName value with prototype scope " + differentUserName);

		User userName1 = (User) applicationContext.getBean("user1");
		userName1.printUserName();
		System.out.println("UserName1 value is singleton " + userName1);
		User differentUserName1 = (User) applicationContext.getBean("user1");
		System.out.println("differentUserName1 value is singleton " + differentUserName1);

		User userName2 = (User) applicationContext.getBean("user2");
		System.out.println("first name: " + userName2.getFirstName() + " last name: " + userName2.getLastName());
		userName2.printUserName();

		UserService userService = (UserService) applicationContext.getBean("userService");
		System.out.println(userService.getUser().getFirstName() + " " + userService.getUser().getLastName());
		System.out.println(userService.getClient().getAddress());
		for (Client client : userService.getClients()) {
			System.out.println("Numele strazii este: " + client.getAddress());
		}
		Set<Entry<User, Client>> entries = userService.getUserMap().entrySet();
		for (Entry<User, Client> entry : entries) {
			System.out.println(entry.getKey().getFirstName()+" "+entry.getValue().getAddress());
		}
		
		Address address = (Address) applicationContext.getBean("address");
		Address address1 = (Address) applicationContext.getBean("address1");
		Address address2 = (Address) applicationContext.getBean("address2");
		Address address3 = (Address) applicationContext.getBean("address3");
		
		System.out.println("street: " + address.getStreet() + ", city: " + address.getCity() + ", country: "+address.getCountry());
		System.out.println("street: " + address1.getStreet() + ", city: " + address1.getCity() + ", country: "+address1.getCountry());
		System.out.println("street: " + address2.getStreet() + ", city: " + address2.getCity() + ", country: "+address2.getCountry());
		System.out.println("street: " + address3.getStreet() + ", city: " + address3.getCity() + ", country: "+address3.getCountry());
		
		/*ArrayList<Client> client5 = (ArrayList<Client>) applicationContext.getBean("client5");
		for (Client myClient : client5) {
		System.out.println(myClient.getAddresses());
		}
		*/
		
		((AbstractApplicationContext) applicationContext).registerShutdownHook();
	}
}
